# SPDX-License-Identifier: GPL-2.0-or-later
MODULE_NAME := error_devices
KO_FILENAME := $(MODULE_NAME).ko
KERNEL_VERS := $(shell uname -r)
KERNEL_PATH := /lib/modules/$(KERNEL_VERS)/build
C_FLAGS     := -std=gnu99 -Wall
PWD         := $(shell pwd)
TARGET_PATH := /lib/modules/$(KERNEL_VERS)/kernel/drivers/char

$(MODULE_NAME)-objs := main.o devices.o
obj-m := $(MODULE_NAME).o
ccflags-y += $(C_FLAGS)

build:
	make -C $(KERNEL_PATH) M=$(PWD) modules

install:
	cp $(KO_FILENAME) $(TARGET_PATH)
	depmod --all
	modprobe $(MODULE_NAME)

uninstall:
	rm $(TARGET_PATH)/$(KO_FILENAME)
	modprobe --remove $(MODULE_NAME)

clean:
	make -C $(KERNEL_PATH) M=$(PWD) clean
	rm test

buildtest:
	gcc -o test -Wall test.c

test: buildtest
	#@./test.sh
	./test
