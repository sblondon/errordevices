/* SPDX-License-Identifier: GPL-2.0-or-later */
#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/uaccess.h>

#include "devices.c"

MODULE_LICENSE("GPL v2");
MODULE_DESCRIPTION("Kernel Errors Pseudo Driver");
MODULE_VERSION("0.1");
MODULE_AUTHOR("Stéphane Blondon");

#define SUCCESS 0

struct error_device_data {
    struct cdev cdev;
    struct file_operations fops;
    char *filename;
};

static struct error_device_data error_devices_data[] = {
    { .fops=eacces_fops, .filename="eacces" },
    { .fops=edeadlk_fops, .filename="edeadlk" },
    { .fops=enoent_fops, .filename="enoent" },
    { .fops=enospc_fops, .filename="enospc" },
    { .fops=erofs_fops, .filename="erofs" },
    { .fops=espipe_fops, .filename="espipe" },
    { .fops=epipe_fops, .filename="epipe" },
};

static int dev_major = 0;
static struct class *errordev_class = NULL;

static int error_device_uevent(struct device *dev, struct kobj_uevent_env *env)
{
    add_uevent_var(env, "DEVMODE=%#o", 0666);
    return SUCCESS;
}

static int __init error_devices_init(void)
{
    int err;
    dev_t dev;
    int devices_qty = sizeof error_devices_data / sizeof error_devices_data[0];

    printk("ERROR_DEV: Init\n");

    err = alloc_chrdev_region(&dev, 0, devices_qty, "errordevs");
    if (err != SUCCESS){
        return -ENOMEM;
    }

    dev_major = MAJOR(dev);

    errordev_class = class_create(THIS_MODULE, "errordevs");
    errordev_class->dev_uevent = error_device_uevent;

    for (int i = 0; i < devices_qty; i++) {
        cdev_init(&error_devices_data[i].cdev, &error_devices_data[i].fops);

        error_devices_data[i].cdev.owner = THIS_MODULE;

        cdev_add(&error_devices_data[i].cdev, MKDEV(dev_major, i), 1);

        device_create(errordev_class, NULL, MKDEV(dev_major, i), NULL, error_devices_data[i].filename);
    }

    return SUCCESS;
}

static void __exit error_devices_exit(void)
{
    int devices_qty = sizeof error_devices_data / sizeof error_devices_data[0];
    for (int i = 0; i < devices_qty; i++) {
        device_destroy(errordev_class, MKDEV(dev_major, i));
    }

    class_unregister(errordev_class);
    class_destroy(errordev_class);

    unregister_chrdev_region(MKDEV(dev_major, 0), MINORMASK);
    printk("ERROR_DEV: exit done\n");
}

module_init(error_devices_init);
module_exit(error_devices_exit);
