#! /bin/bash
# released under GPL v2+
# bash provides messages matching with the kernel errors.

printf "\tRead errors\n"
for DEVICE in eacces enoent epipe espipe
do
 cat /dev/$DEVICE
done

printf "\tWrite errors\n"
for DEVICE in eacces enoent enospc epipe espipe erofs
do
 printf "$DEVICE "
 printf "x" > /dev/$DEVICE
done
