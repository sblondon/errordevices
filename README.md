# ErrorDevices

Linux Kernel module to get virtual devices (like classical `/dev/full` device) raising errors.

This project is _experimental_.

## Examples

```
$ LANG=C echo "x" > /dev/enospc
echo: write error: no space left on device
$ LANG=C echo "x" > /dev/erofs
echo: write error: read-only file system
$ LANG=C cat /dev/epipe
cat: /dev/espipe: Illegal seek
$ LANG=C echo "x" > /dev/epipe
cat: /dev/espipe: Illegal seek
```

## Behaviour

| device      | open   |read    | write  | meaning                  |
|-------------|:------:|:------:|:------:|--------------------------|
| eacces      | EACCES |        |        | Permission denied        |
| enoent      | ENOENT |        |        | No such file or directory|
| enospc      |        | 0      | ENOSPC | No space left on device  |
| epipe       |        | EPIPE  | EPIPE  | Broken pipe              |
| erofs       |        | 0      | EROFS  | Read-only file system    |
| espipe      |        | ESPIPE | ESPIPE | Illegal seek             |


0: provides zero like `/dev/zero`. For example:

```
$ dd if=/dev/enospc of=/tmp/demo bs=1k count=1
```

and watch /tmp/demo content with a hexadecimal editor.


## Implementation needed (?)

| device         | open            | read           | write           | meaning                            |
|----------------|:---------------:|:--------------:|:---------------:|------------------------------------|
| eio            | EIO             | EIO            | EIO             | I/O error                          |
| enxio          | ENXIO           | ENXIO          | ENXIO           | No such device or address          |
| ebadf          | EBADF           | EBADF          | EBADF           | Bad file number                    |
| enotblk        | ENOTBLK         | ENOTBLK        | ENOTBLK         | Block device required              |
| ebusy          | EBUSY           | EBUSY          | EBUSY           | Device or resource busy            |
| eexist         | EEXIST          | EEXIST         | EEXIST          | File exists                        |
| enodev         | ENODEV          | ENODEV         | ENODEV          | No such device                     |
| enotdir        | ENOTDIR         | ENOTDIR        | ENOTDIR         | Not a directory                    |
| eisdir         | EISDIR          | EISDIR         | EISDIR          | Is a directory                     |
| etxtbsy        | ETXTBSY         | ETXTBSY        | ETXTBSY         | Text file busy                     |
| efbig          | EFBIG           | EFBIG          | EFBIG           | File too large                     |
| emlink         | EMLINK          | EMLINK         | EMLINK          | Too many links                     |
| edeadlk        | EDEADLK         | EDEADLK        | EDEADLK         | Resource deadlock would occur      |
| enametoolong   | ENAMETOOLONG    | ENAMETOOLONG   | ENAMETOOLONG    | File name too long                 |
| enotempty      | ENOTEMPTY       | ENOTEMPTY      | ENOTEMPTY       | Directory not empty                |
| eloop          | ELOOP           | ELOOP          | ELOOP           | Too many symbolic links encountered|
| enostr         | ENOSTR          | ENOSTR         | ENOSTR          | Device not a stream                |
| enodata        | ENODATA         | ENODATA        | ENODATA         | No data available                  |
| enolink        | ENOLINK         | ENOLINK        | ENOLINK         | Link has been severed              |
| enotuniq       | ENOTUNIQ        | ENOTUNIQ       | ENOTUNIQ        | Name not unique on network         |
| ebadfd         | EBADFD          | EBADFD         | EBADFD          | File descriptor in bad state       |
| estrpipe       | ESTRPIPE        | ESTRPIPE       | ESTRPIPE        | Streams pipe error                 |
| enosock        |                 | 0              |                 | Socket operation on non-socket     |
| estale         | ESTALE          | ESTALE         | ESTALE          | Stale NFS file handle              |
| enotname       | ENOTNAM         | ENOTNAM        | ENOTNAM         |Not a XENIX named type file         |
| eisnam         | EISNAM          | EISNAM         | EISNAM          | Is a named type file               |
| edquot         | EDQUOT          | EDQUOT         | EDQUOT          | Quota exceeded                     |
| enomedium      | ENOMEDIUM       | ENOMEDIUM      | ENOMEDIUM       | No medium found                    |
| enotrecoverable| ENOTRECOVERABLE | ENOTRECOVERABLE| ENOTRECOVERABLE |State not recoverable               |


## How to use

- clone repository
- `make build`
- `make install` # with root rights

Remove module with `make uninstall`.

## License
Released under GPL version 2 or upper

## Error codes

List is available in `errno` manpage.
