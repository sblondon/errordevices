/* SPDX-License-Identifier: GPL-2.0-or-later */
#include <linux/fs.h>
#include <linux/vmalloc.h>

static int default_open(struct inode *inode, struct file *file);
static int default_release(struct inode *inode, struct file *file);
static long default_ioctl(struct file *file, unsigned int cmd, unsigned long arg);
static ssize_t default_read(struct file *file, char __user *buf, size_t count, loff_t *offset);
static ssize_t default_write(struct file *file, const char __user *buf, size_t count, loff_t *offset);
static int eacces_open(struct inode *inode, struct file *file);
static int edeadlk_lock(struct file *file, int i, struct file_lock *file_lock);
static int enoent_open(struct inode *inode, struct file *file);
static ssize_t espipe_read(struct file *file, char __user *buf, size_t count, loff_t *offset);
static ssize_t espipe_write(struct file *file, const char __user *buf, size_t count, loff_t *offset);
static ssize_t enospc_write(struct file *file, const char __user *buf, size_t count, loff_t *offset);
static ssize_t erofs_write(struct file *file, const char __user *buf, size_t count, loff_t *offset);
static ssize_t epipe_read(struct file *file, char __user *buf, size_t count, loff_t *offset);
static ssize_t epipe_write(struct file *file, const char __user *buf, size_t count, loff_t *offset);


static const struct file_operations espipe_fops = {
    .owner      = THIS_MODULE,
    .open       = default_open,
    .release    = default_release,
    .unlocked_ioctl = default_ioctl,
    .read       = espipe_read,
    .write      = espipe_write,
};

static const struct file_operations eacces_fops = {
    .owner      = THIS_MODULE,
    .open       = eacces_open,
    .release    = default_release,
    .unlocked_ioctl = default_ioctl,
};

static const struct file_operations edeadlk_fops = {
    .owner      = THIS_MODULE,
    .open       = default_open,
    .release    = default_release,
    .unlocked_ioctl = default_ioctl,
    .read       = default_read,
    .write      = default_write,
    .lock       = edeadlk_lock,
};

static const struct file_operations enoent_fops = {
    .owner      = THIS_MODULE,
    .open       = enoent_open,
    .release    = default_release,
    .unlocked_ioctl = default_ioctl,
};

static const struct file_operations enospc_fops = {
    .owner      = THIS_MODULE,
    .open       = default_open,
    .release    = default_release,
    .unlocked_ioctl = default_ioctl,
    .read       = default_read,
    .write      = enospc_write,
};

static const struct file_operations erofs_fops = {
    .owner      = THIS_MODULE,
    .open       = default_open,
    .release    = default_release,
    .unlocked_ioctl = default_ioctl,
    .read       = default_read,
    .write      = erofs_write
};

static const struct file_operations epipe_fops = {
    .owner      = THIS_MODULE,
    .open       = default_open,
    .release    = default_release,
    .unlocked_ioctl = default_ioctl,
    .read       = epipe_read,
    .write      = epipe_write,
};


static int default_open(struct inode *inode, struct file *file)
{
    return 0;
}

static int default_release(struct inode *inode, struct file *file)
{
    return 0;
}

static long default_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
    return 0;
}

static ssize_t default_read(struct file *file, char __user *buf, size_t count, loff_t *offset)
{
    char *zero_ptr;
    char res;

    zero_ptr = (char*) vmalloc(count);
    memset(zero_ptr, 0x00, count);
    res = copy_to_user(buf, zero_ptr, count);
    if (res != 0){
        return -EFAULT;
    }

    return count;
}

static ssize_t default_write(struct file *file, const char __user *buf,
			  size_t count, loff_t *ppos)
{
	return count;
}

static int eacces_open(struct inode *inode, struct file *file)
{
    return -EACCES;
}

static int edeadlk_lock(struct file *file, int i, struct file_lock *file_lock)
{
    return -EDEADLK;
}

static int enoent_open(struct inode *inode, struct file *file)
{
    return -ENOENT;
}

static ssize_t enospc_write(struct file *file, const char __user *buf, size_t count, loff_t *offset)
{
    return -ENOSPC;
}

static ssize_t epipe_read(struct file *file, char __user *buf, size_t count, loff_t *offset)
{
    return -EPIPE;
}

static ssize_t epipe_write(struct file *file, const char __user *buf, size_t count, loff_t *offset)
{
    return -EPIPE;
}

static ssize_t espipe_read(struct file *file, char __user *buf, size_t count, loff_t *offset)
{
    return -ESPIPE;
}

static ssize_t espipe_write(struct file *file, const char __user *buf, size_t count, loff_t *offset)
{
    return -ESPIPE;
}

static ssize_t erofs_write(struct file *file, const char __user *buf, size_t count, loff_t *offset)
{
    return -EROFS;
}
