/* SPDX-License-Identifier: GPL-2.0-or-later */
#include <stdio.h>
#include <assert.h>
#include <errno.h>

int fail_not_found(char *path){
  printf("Error: %s not found.\n", path);
  return 1;
}

int test_espipe(){
   FILE *fp;
   errno = 0;
   char *path = "/dev/espipe";
   fp = fopen(path, "w+");
   if (fp == NULL){
     return fail_not_found(path);
   }

   fprintf(fp, "Testing...\n");

   assert(errno == ESPIPE);

   fclose(fp);
   return 0;
}

int test_eacces(){
   errno = 0;
   char *path = "/dev/eacces";
   fopen(path, "r");
   // correct behaviour does fp == NULL

   assert(errno == EACCES);

   return 0;
}

int test_enospc(){
   FILE *fp;
   errno = 0;
   char *path = "/dev/enospc";
   fp = fopen(path, "w+");
   if (fp == NULL){
     return fail_not_found(path);
   }

   fprintf(fp, "Testing...\n");

   assert(errno == ENOSPC);

   fclose(fp);
   return 0;
}

int test_enoent(){
   errno = 0;
   char *path = "/dev/enoent";
   fopen(path, "w+");
   // correct behaviour: fopen returns NULL

   assert(errno == ENOENT);

   return 0;
}

int test_erofs(){
   FILE *fp;
   errno = 0;
   char *path = "/dev/erofs";
   fp = fopen(path, "w+");
   if (fp == NULL){
     return fail_not_found(path);
   }

   fprintf(fp, "Testing...\n");

   assert(errno == EROFS);

   fclose(fp);

   return 0;
}

int main() {
   int errors = 0;
   errors += test_espipe();
   errors += test_eacces();
   errors += test_enospc();
   errors += test_enoent();
   errors += test_erofs();

   if (errors == 0){
      printf("Success.\n");
   }
   else {
     printf("Fail.\n");
   }
   return 0;
}
